<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Appel les différentes fonctions de pre-remplacement dans le message_retour
 * @param array{data: string} $flux
 * @return array{data: string} $flux
**/
function formidable_retour_etendu_formidable_pre_raccourcis_arobases(array $flux): array {
	include_spip('inc/saisies');

	$data = &$flux['data'];
	if (!$data) {
		return $flux;
	}
	$data = formidable_retour_etendu_afficher_si($data);
	$data = formidable_retour_etendu_objet_info($data);
	return $flux;
}

/**
 * Detecte les
 * ```
 * <condition si="<afficher_si>">
 * partie en affichage conditionnelle
 * </condition>
 * ```
 * et remplace le cas échéant
 * @param string $data
 * @return string
**/
function formidable_retour_etendu_afficher_si(string $data): string {
	$regexp = '#<condition(\s*)'
		. 'si=(?<guillemet>"|\')'
		. '(?<condition>.*)'
		. '(\k<guillemet>)>'
		. '(.*)'
		. '</condition>'
		. '#Us';
	// Vérifier si les conditions sont remplis
	if (preg_match_all($regexp, $data, $matches, PREG_SET_ORDER)) {
		foreach ($matches as $m) {
			$m['condition'] = html_entity_decode($m['condition']);
			if (!saisies_evaluer_afficher_si($m['condition'])) {
				$data = str_replace($m[0], '', $data);
			} else {
				$remplace = trim($m[5]);
				$remplace = preg_replace('#^' . _AUTOBR . '#', '', $remplace) ?? '';
				$remplace = preg_replace('#' . _AUTOBR . '$#', '', $remplace) ?? '';
				$data = str_replace($m[0], $remplace, $data);
			}
		}
	};

	// Nettoyage
	$data = str_replace('<p></p>', '', $data);
	return $data;
}

/**
 * Permet de trouver les @champ_formidable_xx@:CHAMP_OBJET
 * @param string $data
 * @return string $data
**/
function formidable_retour_etendu_objet_info(string $data): string {

	$data = preg_replace('/@:(<span class="caps">([A-Z]*?)<\/span>)/mU', '@:${2}', $data);// Pour orthotypo
	$regexp = '/@((.*)_[0-9]?)@:([A-Z_]*?)/mU';

	if (preg_match_all($regexp, $data, $matches, PREG_SET_ORDER)) {
		foreach ($matches as $m) {
			$a_remplacer = $m[0];
			$champ_sql = strtolower($m[3]);
			$objet = str_replace('selecteur_', '', $m[2]); // A faire mieux un de ces 4
			$objet = objet_type($objet);
			$champ_formidable = $m[1];
			if ($champ_sql) {
				$id = saisies_request($champ_formidable);
				$info = generer_objet_info($id, $objet, $champ_sql);
				$data = str_replace($a_remplacer, $info, $data);
			}
		}
	}
	return $data;
}

/**
 * Complète l'aide mémoire avec de l'aide
 * @param array $flux{arg: array, data: array}
 * @return array $flux{arg: array, data: array}
**/
function formidable_retour_etendu_saisies_aide_memoire_inserer_debut(array $flux): array {
	if ($flux['args']['type_aide_memoire'] === 'formidable_configurer') {
		$message = '<p>' . _T('formidable_retour_etendu:aide_memoire_complement') . '</p>';
		$data = &$flux['data'];
		$data = $message . $data;
	}
	return $flux;
}

/**
 * Puisqu'on utilise les afficher_si, il faut permettre de pouvoir
 * mettre dans un retour / une config de formulaire des raccourcis @@ en plus de simples champs de formulaire supporté par formidable$
 * @param array $flux
 * @return array $flux
 **/
function formidable_retour_etendu_verifier_formidable_coherence_arobase(array $flux): array {
	$data = &$flux['data'];
	$saisies = $flux['args']['saisies'];
	include_spip('inc/saisies');

	$data = array_filter($data, function ($d) use ($saisies) {
		return !saisies_verifier_coherence_afficher_si_par_champ($d, $saisies);
	});

	return $flux;
}
