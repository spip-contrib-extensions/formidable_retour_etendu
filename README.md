# Retour étendu avec Formidable

Ce plugin permet plus de souplesse dans le message de retour de formidable.


Documentation complète sur
https://contrib.spip.net/Formidable-retour-etendu
# Tests unitaires
Pour executer avec PHPunit les tests unitaires


1. `composer install` (la tout première fois)
2. `vendor/bin/phpunit` pour faire tout les tests
3. `XDEBUG_MODE=coverage vendor/bin/phpunit tests/ --coverage-html coverage` pour avoir un rapport de couverture, puis ouvrir `coverage/index.html` avec un navigateur.
