# Changelog
## 1.1.3 - 2025-02-07

### Fixed

- Compatibilité formidable 7.0.0
## 1.1.2 - 2025-02-07

### Fixed

- Faire fonctionner les tests contenant des caractères susceptibles d'être encodés en entités html par `propre()` (`&`, `>`, `<` principalement)

## 1.1.1 - 2024-06-09

### Unreleased

- Compatible SPIP 4++
## 1.1.0 - 2023-12-22

### Added

- La syntaxe `@champ_formidable_xx@:CHAMP_SQL` pour trouver la valeur d'un champ SQL de l'objet envoyé en réponse de formulaire.

## 1.0.2 - 2023-03-18

## Fix

- Compatibilité SPIP 4.2

## 1.0.1 - 2022-07-05

### Fix

- Corrections diverses sur l'expression régulière pour les affichages conditionnelles

## 1.0.0 - 2022-05-31

### Added

- Sorti du plugin
