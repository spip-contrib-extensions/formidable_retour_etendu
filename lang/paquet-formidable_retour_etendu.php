<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'formidable_retour_etendu_description' => 'Ce plugin permet d\'ajuster finement le contenu des message de retours dans formidable, en permettant d\'afficher certaines parties sous conditions.',
	'formidable_retour_etendu_slogan' => 'Soyez précis dans vos retour !'
);
