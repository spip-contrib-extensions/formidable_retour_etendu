<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'aide_memoire_complement' => 'Vous pouvez utiliser les fonctionnalités du plugin <a target="_blank" href="https://contrib.spip.net/Formidable-retour-etendu">Formidable retour étendu</a>.',
);
