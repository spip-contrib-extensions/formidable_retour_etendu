<?php

namespace Spip\FormidableRetourEtendu\Test;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 */

class PipelineTest extends TestCase {

	public static function dataAfficherSi() {

		return [
			'false' => [
				"Avant\r\n"
				. 'Apres',
				"Avant\r\n"
				. "<condition si='false'>"
				. "F"
				. "</condition>"
				. 'Apres',
			],
			'false_double_quote' => [
				"Avant\r\n"
				. 'Apres',
				"Avant\r\n"
				. "<condition si=\"false\">"
				. "F"
				. "</condition>"
				. 'Apres',
			],
			'true' => [
				"Avant\r\n"
				. "True\r\n\r\n"
				. "Apres"
				,
				"<p></p>Avant\r\n"
				. "<condition si='true'>"
				. "True\r\n"
				. "<br></condition>\r\n"
				. 'Apres',
			],
			'true_double_quote' => [
				"Avant\r\n"
				. "True\r\n\r\n"
				. "Apres"
				,
				"<p></p>Avant\r\n"
				. "<condition si=\"true\">"
				. "True\r\n"
				. "<br></condition>\r\n"
				. 'Apres',
			],
		];
	}

	/**
	 * @dataProvider dataAfficherSi
	 * @covers formidable_retour_etendu_afficher_si()
	**/
	public function testsAfficherSi($expected, $input) {
		$actual = formidable_retour_etendu_afficher_si($input);
		$this->assertEquals($expected, $actual);
	}

	public static function dataObjetInfo() {
		return [
			'ca_marche' => [
				'Le titre est info_titre_article_2. Celui evt: info_titre_evenement_2. Et champ avec underscore.',
				'Le titre est @selecteur_article_1@:TITRE. Celui evt: @evenements_1@:<span class="caps">TITRE</span>. Et @evenements_1@:CHAMP_UNDERSCORE.'
			],
			'ca_marche_pas' => [
				'Le titre est @articles_1@:titre.',
				'Le titre est @articles_1@:titre.'
			]
		];
	}

	/**
	 * @dataProvider dataObjetInfo
	 * @covers formidable_retour_etendu_objet_info()
	**/
	public function testsObjetInfo($expected, $input) {
		$actual = formidable_retour_etendu_objet_info($input);
		$this->assertEquals($expected, $actual);
	}

}
