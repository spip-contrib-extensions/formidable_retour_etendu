<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../formidable_retour_etendu_pipelines.php';
// decorate SPIP…
if (!function_exists('include_spip')) {
	function include_spip() {
		return true;
	}
	define('_ECRIRE_INC_VERSION', true);
	define('_AUTOBR', '<br>');
}

if (!function_exists('saisies_evaluer_afficher_si')) {
	function saisies_evaluer_afficher_si(string $condition): bool {
		if ($condition === 'false') {
			return false;
		}
		if ($condition === 'true') {
			return true;
		}
	}
}

if (!function_exists('generer_objet_info')) {
	function generer_objet_info(string $id_objet, string $type_objet, string $info): string {
		$infos = [
			'article' => [
				'2' => [
					'titre' => 'info_titre_article_2'
				]
			],
			'evenement' => [
				'2' => [
					'titre' => 'info_titre_evenement_2',
					'champ_underscore' => 'champ avec underscore'
				]
			]
		];
		return $infos[$type_objet][$id_objet][$info];
	}

	function objet_type(string $objet): string {
		$data = [
			'articles' => 'article',
			'evenements' => 'evenement',
		];
		return $data[$objet] ?? $objet;
	}

	function saisies_request(string $req): string {
		$post = [
			'articles_1' => '2',
			'evenements_1' => '2',
			'selecteur_article_1' => '2'
		];
		return $post[$req];
	}
}
